import React from "react";
import ReactDOM from "react-dom";
import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Dashboard from "./js/components/layout/Dashboard.js"

const theme = createMuiTheme({
  palette: {
    type: 'light',
  },
});


const App = () => {
  return <ThemeProvider theme={theme}>
    <Dashboard />
  </ThemeProvider>;
};

ReactDOM.render(<App />, document.querySelector("#root"));